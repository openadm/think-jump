<?php
namespace openadm\think\traits;

use think\App;
use think\exception\HttpResponseException;
use think\Response;
use think\facade\Request;

/**
 * Jump 跳转类
 * @package think\traits\Jump
 */
trait Jump
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
    }

    /**
     * 成功跳转
     * @param string $msg 信息
     * @param string $url 跳转地址
     * @param int $wait 等待时间
     * @param array $header Header头
     * @param mixed $data 其它数据
     */
    public function success(string $msg, string $url = null, int $wait = null, array $header = [], $data = null)
    {

        // URL处理
        if (null === $url) {
            $url = $this->request->server('HTTP_REFERER', 'javascript:history.back()');
        } else {
            $url = $this->buildUrl($url);
        }

        // 跳转等待时间
        null === $wait && $wait = $this->app->config->get('jump.success_wait');

        $result = [
            'code' => $this->app->config->get('jump.success_code'),
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
            'data' => $data
        ];

        // AJAX则返回JSON
        if (Request::isAjax()) {
            $response = Response::create($result, $this->getAjaxReturn())->header($header);
        } else {
            $response = Response::create($this->app->config->get('jump.success_tmpl'), 'view')->header($header)->assign($result);
        }

        throw new HttpResponseException($response);
    }

    /**
     * 错误跳转
     * @param string $msg 错误信息
     * @param string $url 跳转地址
     * @param int $wait 等待时间
     * @param array $header Header头
     * @param mixed $data 其它数据
     */
    public function error(string $msg, string $url = null, int $wait = null, array $header = [], $data = null)
    {
        // URL处理
        if (null === $url) {
            $url = 'javascript:history.back()';
        } else {
            $url = $this->buildUrl($url);
        }

        // 跳转等待时间
        null === $wait && $wait = $this->app->config->get('jump.error_wait');

        $result = [
            'code' => $this->app->config->get('jump.error_code'),
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
            'data' => $data
        ];

        // AJAX则返回JSON
        if (Request::isAjax()) {
            $response = Response::create($result, $this->getAjaxReturn())->header($header);
        } else {
            $response = Response::create($this->app->config->get('jump.error_tmpl'), 'view')->header($header)->assign($result);
        }

        throw new HttpResponseException($response);
    }

    /**
     * 403跳转
     * @param string $msg 错误信息
     * @param string $url 跳转地址
     * @param int $wait 等待时间
     * @param array $header Header头
     * @param mixed $data 其它数据
     */
    public function forbidden(string $msg, string $url = null, int $wait = null, array $header = [], $data = null)
    {
        // URL处理
        if (null === $url) {
            $url = 'javascript:history.back()';
        } else {
            $url = $this->buildUrl($url);
        }

        // 跳转等待时间
        null === $wait && $wait = $this->app->config->get('jump.error_wait');

        $result = [
            'code' => $this->app->config->get('jump.forbidden_code'),
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
            'data' => $data
        ];

        // AJAX则返回JSON
        if (Request::isAjax()) {
            $response = Response::create($result, $this->getAjaxReturn())->header($header);
        } else {
            $response = Response::create($this->app->config->get('jump.error_tmpl'), 'view')->header($header)->assign($result);
        }

        throw new HttpResponseException($response);
    }

    /**
     * 401跳转
     * @param string $msg 错误信息
     * @param string $url 跳转地址
     * @param int $wait 等待时间
     * @param array $header Header头
     * @param mixed $data 其它数据
     */
    public function unauthorized(string $msg, string $url = null, int $wait = null, array $header = [], $data = null)
    {
        // URL处理
        if (null === $url) {
            $url = 'javascript:history.back()';
        } else {
            $url = $this->buildUrl($url);
        }

        // 跳转等待时间
        null === $wait && $wait = $this->app->config->get('jump.error_wait');

        $result = [
            'code' => $this->app->config->get('jump.unauthorized_code'),
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
            'data' => $data
        ];

        // AJAX则返回JSON
        if (Request::isAjax()) {
            $response = Response::create($result, $this->getAjaxReturn())->header($header);
        } else {
            $response = Response::create($this->app->config->get('jump.error_tmpl'), 'view')->header($header)->assign($result);
        }

        throw new HttpResponseException($response);
    }

    /**
     * 页面重定向
     * @param string $url 重定向地址
     * @param string $msg 消息
     * @param int $code 状态码
     * @param array $header Header头
     */
    public function redirect(string $url = null, string $msg = '', int $code = 302, array $header = [])
    {
        // URL处理
        $url = $this->buildUrl($url);

        // AJAX则返回JSON
        if (Request::isAjax()) {
            $result = [
                'code' => $code,
                'msg'  => $msg,
                'url'  => $url,
            ];
            $response = Response::create($result, $this->getAjaxReturn());
        } else {
            $response = Response::create($url, 'redirect', $code);
        }

        throw new HttpResponseException($response->header($header));
    }

    /**
     * 返回封装后的API数据
     * @param mixed $data 数据
     * @param mixed $code 状态
     * @param string $msg 提示信息
     * @param string $type 数据类型
     * @param array $header Header头
     */
    public function result($data, $code = null, $msg = '', string $type = null, array $header = [])
    {

        $result = [
            'code' => $code ?? $this->app->config->get('jump.result_code'),
            'msg'  => $msg,
            'time' => time(),
            'data' => $data,
        ];

        $response = Response::create($result, $type ?? $this->getAjaxReturn())->header($header);

        throw new HttpResponseException($response);
    }

    /**
     * URL地址生成
     * @param string $url
     * @return string
     */
    public function buildUrl(string $url = null)
    {
        if(null === $url) {
            $url = $this->request->server('HTTP_REFERER', '/');
        } elseif (preg_match('@^([a-zA-Z0-9-]+://|/|javascript:)@', $url)) {
            return $url;
        }

        return (string)$this->app->route->buildUrl($url);
    }

    /**
     * 获取AJAX请求返回数据格式，根据客户端接受的数据类型自动判断
     * @return string
     */
    protected function getAjaxReturn()
    {
        $type = Request::type();
        switch ($type) {
            case 'json':
            case 'xml':
                return $type;
                break;
            case 'js':
                return 'jsonp';
                break;
            default:
                return $this->app->config->get('jump.ajax_return');
                break;
        }
    }
}