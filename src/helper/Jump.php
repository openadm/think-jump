<?php
namespace openadm\think\helper;

use think\App;

/**
 * Jump 跳转类
 * @package think\helper\Jump
 */
class Jump
{
    use \openadm\think\traits\Jump;

    public static function __make(App $app)
    {
        return new static($app);
    }
}