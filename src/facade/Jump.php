<?php
namespace openadm\think\facade;

use think\Facade;

/**
 * @see \think\Config
 * @package think\facade
 * @mixin \think\Config
 */
class Jump extends Facade
{
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'openadm\think\helper\Jump';
    }
}
