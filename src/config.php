<?php
// 跳转配置
return [
    // 成功跳转页面模板文件
    'success_tmpl' => app()->getRootPath() . 'vendor/openadm/think-jump/src/tpl/success.html',
    // 成功跳转页停留时间(秒)
    'success_wait' => 1,
    // 成功跳转的code值
    'success_code' => 200,
    // 错误跳转页面模板文件
    'error_tmpl'   => app()->getRootPath() . 'vendor/openadm/think-jump/src/tpl/error.html',
    // 错误跳转页停留时间(秒)
    'error_wait'   => 3,
    // 错误跳转的code值
    'error_code'   => 300,
    'forbidden_code'  => 403,
    //Unauthorized
    'unauthorized_code' => 401,
    // 默认AJAX请求返回数据格式，可用：Json,Jsonp,Xml
    'ajax_return' => 'Json',
];
